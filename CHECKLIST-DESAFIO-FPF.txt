============================CHECKLIST DESAFIO FPF============================
> [OK] CRIAÇÃO DE UMA API RESTful
    > [OK] Objeto |Player|;
        -playerName: string;
        -data: Date;
        -score: number;
-----------------------------------------------------------------------------
> CRIAÇÃO DE UM MENU DE NAVEGAÇÃO:
    > [OK] HOME - Tela de Boas-Vindas;
    > [OK] Iniciar Jogo - Tela de Jogo;
    > Regras do Jogo - Texto explicativo com as regras do jogo
    > [OK] Ranking - Listagem da pontuação dos jogadores em ordem decrescente.
-----------------------------------------------------------------------------
> REGRAS DO JOGO
    > [OK] Jogo em turnos, uma vez o jogador e outra o inimigo;
    > [OK] Ambos possuim uma barra de vida (100 pontos de vida);
    > [OK] O jogador deverá ter 4 botões de interação:
        - Ataque básico: Causa dano ao inimigo (5 - 10 de dano);
        - Ataque Especial: Causa dano ao inimigo (10 - 20 de dano) |
            CD de 2 turnos | 50% de chance de atordoar;
        -Curar: recupera vida (5 - 15 vida recuperada);
        -Desistir: Jogo deverar ser terminado e ir para tela HOME;
    > [OK] Ações do inimigo:
        - Todas as ações deveram ocorrer de forma automática, logo após o
            jogador realizar sua ação;
        - Se barra de vida do inimigo chegar a zero ele não poderá mais
            realizar uma ação;
        - Ataque básico: causa dano ao jogador (6 - 12 de dano);
        - Ataque Especial: causa dano ao jogador (8 - 16 de dano)
            CD de 3 turnos;
-----------------------------------------------------------------------------
> [OK] INDICADOR DE VIDA
    > [OK] Tanto o jogador quanto o inimigo deverão ter a cor da barra de vida de
        acordo com:
            - Verde: >= 50% de vida;
            - Amarelo: < 50% de vida;
            - Vermelho: < 20% de vida;
-----------------------------------------------------------------------------
> [OK] LOG DA BATALHA
    > [OK] O jogo deverá conter uma seção de log de acordo com a ação do jogador e
        do inimigo;
-----------------------------------------------------------------------------
> [OK] FINAL DO JOGO
    > [OK] Cálculo da pontuação;
        - (Pontos de vida restantes do jogador*1000)/Número de turnos jogados;
    > [OK] Ao final deverá aparecer uma mensagem com a pontuação final;
    > [OK] O resultado deve ser registrado no ranking através da API Rest,
        o usuário deve informar seu nome;
-----------------------------------------------------------------------------
> [OK] TELA DE Ranking
    > [OK] Ler os dados da API e mostrar a listagem de jogares e suas respectivas
        pontuações em ordem decrescente;
