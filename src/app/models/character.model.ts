import { Debilitation } from './debilitation.model';
import { Sprites } from './sprites.model';
import { Super } from './super.model';
import { MinMax } from './min-max.model';
export interface Character{
  hero: boolean
  name: string
  life: number
  damageBasic: MinMax
  damageSuper: Super
  winner: boolean
  img: Sprites
  heal?: MinMax
  score?: number
  debilitation?: Debilitation
  turn?: boolean
}
