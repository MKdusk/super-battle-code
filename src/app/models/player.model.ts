export interface Player{
  player: string
  data: Date
  score: number
  id?: number
}
