export interface ClassModifier{
  add: string
  remove1: string
  remove2: string
}
