import { Router } from '@angular/router';
import { Character } from './../../models/character.model';
import { GameplayService } from './../../gameplay/gameplay.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero-selection',
  templateUrl: './hero-selection.component.html',
  styleUrls: ['./hero-selection.component.css']
})
export class HeroSelectionComponent implements OnInit {

  public listHero: Character[] = [];
  public listEnemy: Character[] = [];

  constructor(private gameplayService: GameplayService, private router: Router) {

  }

  ngOnInit(): void {
    this.gameplayService.onlyHero(this.listHero);
    this.gameplayService.onlyEnemy(this.listEnemy);
  }

  public startGame(hero: Character): void {
    this.gameplayService.heroSelected = hero;
    this.gameplayService.choiseEnemy(this.listEnemy);
    this.router.navigateByUrl('gameplay');
  }


}
