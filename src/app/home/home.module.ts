import { GameplayModule } from './../gameplay/gameplay.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { StartComponent } from './start/start.component';
import { HeroSelectionComponent } from './hero-selection/hero-selection.component';


@NgModule({
  declarations: [
    HomeComponent,
    StartComponent,
    HeroSelectionComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    GameplayModule
  ],
  exports: [
    HomeComponent,
  ]
})
export class HomeModule { }
