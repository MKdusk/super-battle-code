import { RankingService } from './ranking.service';
import { Component, OnInit } from '@angular/core';
import { Player } from '../models/player.model';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {

  public listPlayers!: Player[];

  constructor(private rankingService: RankingService) { }

  public ngOnInit(): void {
    this.rankingService.playerRanking().
    subscribe({
      next: (players) => this.listPlayers = players.sort((a,b) => b.score - a.score),
      error: (error) => console.log(error)
    })
  }
}
