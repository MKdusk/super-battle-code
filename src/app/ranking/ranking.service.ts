import { Observable } from 'rxjs';
import { Player } from './../models/player.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class RankingService {

  private url = 'http://localhost:3000/player';

  constructor(private httpClient: HttpClient) {}

  public playerRanking(): Observable<Player[]> {
    return this.httpClient.get<Player[]>(this.url);
  }
}
