import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then((m)=>m.HomeModule),
  },
  {
    path: 'gameplay',
    loadChildren: () => import('./gameplay/gameplay.module').then((m)=>m.GameplayModule)
  },
  {
    path: 'ranking',
    loadChildren: () => import('./ranking/ranking.module').then((m)=>m.RankingModule)
  },
  {
    path: 'regras',
    loadChildren: () => import('./regras/regras.module').then((m)=>m.RegrasModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
