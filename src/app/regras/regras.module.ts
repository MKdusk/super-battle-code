import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegrasRoutingModule } from './regras-routing.module';
import { RegrasComponent } from './regras.component';


@NgModule({
  declarations: [
    RegrasComponent
  ],
  imports: [
    CommonModule,
    RegrasRoutingModule
  ]
})
export class RegrasModule { }
