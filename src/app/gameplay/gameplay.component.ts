import { ClassModifier } from './../models/class-modifier';
import { Character } from './../models/character.model';
import { Log } from './../models/log.model';
import { GameplayService } from './gameplay.service';
import { Component, ElementRef, OnInit, ViewChild, Renderer2 } from '@angular/core';
import { timer } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gameplay',
  templateUrl: './gameplay.component.html',
  styleUrls: ['./gameplay.component.css']
})

export class GameplayComponent implements OnInit {
  public player!: Character;

  public enemy!: Character;

  public logBattle: Log[] = [];
  public turns = 0;
  public surrender = false;
  public nick!: string;

  @ViewChild('logBDvi') logDiv!: ElementRef;
  @ViewChild('progressE') progressEnemy!: ElementRef;
  @ViewChild('progressP') progressPlayer!: ElementRef;
  @ViewChild('screen') tela!: ElementRef;


  constructor(private gameplayService: GameplayService,
    private renderer2: Renderer2,
    private router: Router ) { }

  ngOnInit(){
    this.player = this.gameplayService.heroSelected;
    this.enemy = this.gameplayService.enemySelected;
  }


  public resetGame(): void {
    window.location.reload();
  }

  public playerTurn(acao: string): void {

    this.turns++;
    if (this.player.damageSuper.myColldown) {
      this.player.damageSuper.myColldown--;
    }
    if (acao === 'attack') {
      const log = this.gameplayService.attack(this.player, this.enemy);
      this.updateLog(log);
    }
    if (acao === 'super') {
      const log = this.gameplayService.super(this.player, this.enemy);
      this.updateLog(log);
      if (this.enemy.debilitation!.stun) {
        this.updateLog({       class: "alert alert-stun log",
          img: this.enemy.img.icon,
          msg: `${this.enemy.name} sofreu stun!`
        });
      }
    }

    if (acao === 'heal') {
      const log = this.gameplayService.heal(this.player);
      this.updateLog(log);
      this.progressLifeBar(this.player.life, this.progressPlayer);
    } else {
      this.progressLifeBar(this.enemy.life, this.progressEnemy);
    }


    if (!this.existWinner()) {
      this.enemy.turn = true;
      this.enemyTurn();
    } else {
      this.renderer2.addClass(this.tela.nativeElement, "overlay");
    }

    if (this.player.winner) {
      this.player.score = Math.round((this.player.life*1000)/this.turns);
    }
  }

  public enemyTurn(): void {
    timer(1000).subscribe(() => {
      if (!this.enemy.debilitation!.stun) {
        if (!this.enemy.damageSuper.myColldown) {
          const log = this.gameplayService.super(this.enemy, this.player);
          this.updateLog(log);
        } else {
          const log = this.gameplayService.attack(this.enemy, this.player);
          this.updateLog(log);
          this.enemy.damageSuper.myColldown--;
        }
        this.progressLifeBar(this.player.life, this.progressPlayer);
      } else {
        this.enemy.debilitation!.stun--;
      }

      if (!this.existWinner()) {
        this.enemy.turn = false;
      }
    })
  }


  private progressLifeBar(life: number, progressBar: ElementRef): void {
    this.renderer2.setStyle
    (progressBar.nativeElement, 'width', life + "%");

    const colorBar = this.colorBar(life);

    if (colorBar) {
      this.renderer2.addClass(progressBar.nativeElement, colorBar.add);
      this.renderer2.removeClass(progressBar.nativeElement, colorBar.remove1);
      this.renderer2.removeClass(progressBar.nativeElement, colorBar.remove2);
    }
  }

  //criar uma interface pra classes de cor
  private colorBar(life: number): ClassModifier {
    const lifePorcent = (100 * life)/100;

    if (lifePorcent >= 50) {
      return {add: "bg-success", remove1: "bg-warning", remove2: "bg-danger"}
    }
    if (lifePorcent < 50 && lifePorcent > 20) {
      return {add: "bg-warning", remove1: "bg-success", remove2: "bg-danger"};
    }

    return {add: "bg-danger", remove1: "bg-warning", remove2: "bg-success"};
  }

  private updateLog(log: Log): void {
    this.logBattle.push(log);

    timer(1).subscribe(() => {
      this.logDiv.nativeElement.scrollTop = this.logDiv.nativeElement.scrollHeight;
    });
  }

  public surrenderGame(): void {
    this.surrender = !this.surrender;
    if (this.surrender) {
      this.renderer2.addClass(this.tela.nativeElement, "overlay");
    } else {
      this.renderer2.removeClass(this.tela.nativeElement, "overlay");
    }
  }

  private existWinner(): boolean {
    return this.player.winner || this.enemy.winner;
  }

  public submitScore(): void {
    const score = this.player.score as number;
    const rank = {player: this.nick, data: new Date(), score: score};
    this.gameplayService.adicionarRank(rank)
    .subscribe({
      next: () => this.router.navigateByUrl('ranking'),
      error: (error) => console.error(error)
    });
  }
}
