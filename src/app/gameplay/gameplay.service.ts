import { Character } from './../models/character.model';
import { Log } from './../models/log.model';
import { HttpClient } from '@angular/common/http';
import { Player } from './../models/player.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class GameplayService {

  private urlPlayer = 'http://localhost:3000/player';
  private urlCharacter = 'http://localhost:3000/character';
  public heroSelected!: Character;
  public enemySelected!: Character;

  constructor(private httpClient: HttpClient) { }

  public attack(hunter: Character,target: Character): Log {
    const damage = this.getRandomIntInclusive(hunter.damageBasic.min, hunter.damageBasic.max);
    target.life -= damage;
    if(target.life <= 0){
      target.life = 0;
      hunter.winner = true;
    }
    return {
      class: "alert alert-info log",
      img: hunter.img.icon,
      msg: `${hunter.name} ataca ${target.name} causando ${damage} de dano!`
    }
  }

  public super(hunter: Character, target: Character): Log {
    const damage = this.getRandomIntInclusive(hunter.damageSuper.min, hunter.damageSuper.max);
    hunter.damageSuper.myColldown = hunter.damageSuper.colldown;
    target.life -= damage;
    if(target.life <= 0){
      target.life = 0;
      hunter.winner = true;
    }
    if(target.debilitation){
      const stun = this.getRandomIntInclusive(0, 1);
      stun ? target.debilitation.stun = 1 : target.debilitation.stun = 0;
    }

    return {
      class: "alert alert-warning log",
      img: hunter.img.icon,
      msg: `${hunter.name} ataca usando um SUPER ataque em ${target.name} causando ${damage} de dano!`
    }
  }

  public heal(injured: Character): Log {
    const cure = this.getRandomIntInclusive(injured.heal!.min, injured.heal!.max);
    injured.life += cure;

    if (injured.life > 100) {
      injured.life = 100;
    }

    return {
      class: "alert alert-success log",
      img: injured.img.icon,
      msg: `${injured.name} utilizou crtl+z e recebeu ${cure} de cura!`
    }
  }

  private getRandomIntInclusive(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  public adicionarRank(player: Player): Observable<Player> {
    return this.httpClient.post<Player>(this.urlPlayer, player);
  }

  get character(): Observable<Character[]> {
    return this.httpClient.get<Character[]>(this.urlCharacter);
  }

  public onlyHero(listHero: Character[]): void {
    this.character.subscribe((character: Character[])=>{
      character.forEach(function (char) {
        if (char.hero) {
          listHero.push(char);
        }
      });
    })
  }

  public onlyEnemy(listEnemy: Character[]): void {
    this.character.subscribe((character: Character[])=>{
      character.forEach(function (char) {
        if(!char.hero){
          listEnemy.push(char);
        }
      })
    })
  }

  public choiseEnemy(listEnemy: Character[]): void {
    const indexEnemy = this.getRandomIntInclusive(0, listEnemy.length-1);
    this.enemySelected = listEnemy[indexEnemy];
  }

}
