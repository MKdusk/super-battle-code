import { FormsModule } from '@angular/forms';
import { NgModule, Renderer2 } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameplayRoutingModule } from './gameplay-routing.module';
import { GameplayComponent } from './gameplay.component';


@NgModule({
  declarations: [
    GameplayComponent,
  ],
  imports: [
    CommonModule,
    GameplayRoutingModule,
    FormsModule
  ],
  exports: [
    GameplayComponent
  ]
})
export class GameplayModule { }
