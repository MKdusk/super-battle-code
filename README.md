# SuperBattleCode

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.2. and [Node](https://nodejs.org/en/download/) version v16.15.0.

## Preparing the environment

All commands must be done inside the project paste.

Run `npm install`;
 
Run `npm install -g json-server`;   

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Start JSON Server

Run `json-server --watch bd.json` for a database serve.

## Images

All assets images used for this project was made by me.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
